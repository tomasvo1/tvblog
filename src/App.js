import React, { Component } from 'react';
import siteRoutes from './siteRoutes';
import { Route } from 'react-router-dom';

import {
  Main, Inner
} from './Containers';

class App extends Component {
  render() {
    return (
      <div>
        <Route exact path={siteRoutes.MAIN} component={Main} />
        <Route path={siteRoutes.INNER} component={Inner} />
      </div>
    );
  }
}

export default App;
