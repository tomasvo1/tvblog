import React, { Component } from 'react';
import { Container, Header } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import './InnerStyles.css';

export default class Inner extends Component {

    constructor(props) {
        super(props);
        this.state = { name: '', email: '', body: '' };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        const newComment = { name: this.state.name, email: this.state.email, body: this.state.body };
        localStorage.setItem('newComment', JSON.stringify(newComment));
        localStorage.setItem('post', JSON.stringify(this.props.location.state.post));
        localStorage.setItem('comments', JSON.stringify(this.props.location.state.matched));
        const retrievedNewComment = localStorage.getItem('newComment');
        const retrievedPost = localStorage.getItem('post');
        const retrievedComments = localStorage.getItem('comments');
        console.log('retrievedObject: ', JSON.parse(retrievedNewComment));
        console.log('retrievedPost: ', JSON.parse(retrievedPost));
        console.log('retrievedComments: ', JSON.parse(retrievedComments));
    }


    render() {
        let comment = '';
        let post = { title: '', body: '' };
        if (this.props.location.state) {
            comment = this.props.location.state.matched;
        }
        if (this.props.location.state && this.props.location.state.post) {
            post = this.props.location.state.post;
        }

        const commentElement = !comment ? null : (
                comment.map(match => {
                    return (
                    <div className="content">
                        <Container key={match.id}>
                            <div className="ui raised segment">
                                <Header as='h3'>{match.email}</Header>
                                <Header as='h3'>{match.body}</Header>
                            </div>
                        </Container>
                    </div>
                )
            })
        );

        return (
            <div>
                <Container>
                    <div className="postWrap">
                        <div className="ui raised segment">
                            <Header as='h2'>{post.title}</Header>
                            <p>{post.body} </p>
                        </div>
                    </div>
                </Container>
                <Container>
                    <div className="ui horizontal divider">
                        Comments
                </div>
                </Container>
                {
                    commentElement
                }
                <Container>
                    <div className="ui horizontal divider">
                        Add comment
                </div>
                </Container>


                <div className="addCommentField">
                    <Container>
                        <form className="ui form">
                            <div className="field">
                                <label>Comment name</label>
                                <input placeholder="Enter comment name" type="text" value={this.state.name} onChange={e => this.setState({ name: e.target.value })} />
                            </div>
                            <div className="E-mail">
                                <label>E-mail address</label>
                                <input placeholder="Enter your e-mail address" type="text" value={this.state.email} onChange={e => this.setState({ email: e.target.value })} />
                            </div>
                            <div className="field">
                                <label>Comment</label>
                                <textarea value={this.state.body} onChange={e => this.setState({ body: e.target.value })} ></textarea>
                            </div>
                            <button className="ui button" type="submit" onClick={this.handleSubmit}>Submit</button>
                        </form>
                    </Container>
                </div>
            </div>
        )
    }

}