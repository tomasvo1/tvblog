import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container, Header } from 'semantic-ui-react';
import './MainStyles.css';

export default class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            comments: []
        }
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => res.json())
            .then(data => this.setState({ posts: data }));
        fetch('https://jsonplaceholder.typicode.com/comments')
            .then(res => res.json())
            .then(data => this.setState({ comments: data }));
    }

    render() {
        const { posts, comments } = this.state;

        return (
            <div>
                {
                    posts.map(post => {
                        const matched = [];
                        comments.forEach(comment => {
                            if (post.id === comment.postId)
                                matched.push(comment);
                        });
                        console.log(matched);
                        console.log(post);
                        return (
                            <div className="content">
                                <Container key={post.id}>
                                    <div className="ui raised segment">
                                        <Header as='h2'>{post.title}</Header>
                                        <Header as='h3'>By: {post.id}</Header>
                                        <p>{post.body} </p>
                                        <p>Comments: {matched.length} </p>
                                        <div className="wrap">
                                            <Link to={{ pathname: `/inners/${post.id}`, state: { matched: matched, post: post } }}>Click for comments</Link>
                                        </div>
                                    </div>
                                </Container>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

}
